package main

import (
	"flag"
	"fmt"
	"os"
	"strings"
	"time"
)

// Dummy tool to replace youtube-dl when testing
// Pass -ytdlcmd "go run cmd/dummytdl/main.go"

func main() {
	var op string
	var title string
	flag.StringVar(&op, "output", "", "path to save file")
	flag.StringVar(&title, "get-title", "", "title")
	flag.Bool("newline", false, "print update as lines")
	flag.Parse()

	if title != "" {
		time.Sleep(2 * time.Second)
		fmt.Println("Dummy title")
		return
	}

	op = fmt.Sprintf("%s.mp4", strings.TrimSuffix(op, ".%(ext)s"))
	os.WriteFile(op, []byte("slkfjsdlkfdjkfj"), 0644)
	i := 0
	for range time.Tick(1 * time.Second) {
		fmt.Printf("Hello msg %d\n", i)
		i++
		if i == 7 {
			break
		}
	}
}
