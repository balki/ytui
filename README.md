YTUI - Front end for youtube-dl
-------------------------------

Bare minimum zero dependency¹² single binary web app to download youtube videos using youtube-dl. Allows multiple downloads in parallel. Shows title and live download progress.
Pull requests welcome. For a more feature full nicer looking app, checkout [YoutubeDL-Material](https://github.com/Tzahi12345/YoutubeDL-Material)

¹Uses `golang.org/x/net` for websockets. It is strictly not part of go standard library, but still from official go.
²Uses bootstrap for front-end. Files are embedded in the binary, does not make external requests both in front-end and back-end. See CSP header in [templates/index.html](templates/index.html#L6)

Install
-------

    ❯ go install gitlab.com/balki/ytui@latest
    go: downloading gitlab.com/balki/ytui v0.0.0-...

If you don't have and don't want to install go tool-chain, use docker to get the binary

    ❯ docker run --rm -v "${PWD}:/op" golang:latest sh -c "go install gitlab.com/balki/ytui@latest && cp -v /go/bin/ytui /op/"
    go: downloading gitlab.com/balki/ytui v0.0.0-...
    go: downloading golang.org/x/net v0.0.0-...
    '/go/bin/ytui' -> '/op/ytui'

Usage
-----

    ❯ ~/go/bin/ytui -h
    2022/11/21 11:30:40 Youtube UI
    Usage of ./ytui:
      -assetspath string
            Path where css files are saved and served (default "./assets")
      -cachepath string
            Path where temporary download files are saved (default "./cache")
      -dbpath string
            Path where downloaded info is saved (default "./db.json")
      -port int
            Port to listen on (default 8080)
      -saveassets
            Should the assets be saved in dir, so can be served by web server
      -title string
            Title of the page (default "Youtube Downloader")
      -videospath string
            Path where videos are saved (default "./vids")
      -videosurl string
            Prefix of the url, i.e. https://domain.com/<this var>/<video filename> (default "/vids")
      -ytdlcmd string
            youtube-dl command seperated by spaces (default "youtube-dl")

My Deployment setup
-------------------

## File: /usr/local/lib/systemd/system/ytui.service

    [Unit]
    Description=Youtube UI
    
    [Service]
    User=ytui
    Type=simple
    StateDirectory=ytui
    WorkingDirectory=/var/lib/ytui
    ExecStart=/path/to/ytui -ytdlcmd "youtube-dl --proxy socks5://x.x.x.x:1080"
    
    [Install]
    WantedBy=multi-user.target

## File: /usr/local/lib/sysusers.d/ytui.conf

    # See sysusers.d(5) for details.
    
    u ytui - "Youtube UI"

## File: /etc/caddy/conf.d/ytui.conf
 
Make sure your main caddy file includes this

    ytui.mydomain.com {
        root * /var/lib/ytui
    
        handle /vids/* {
            file_server
        }
    
        handle {
            reverse_proxy 127.0.0.1:8080
        }
    }

## Setup Commands

    sudo systemctl daemon-reload
    sudo systemctl restart systemd-sysusers
    sudo systemctl start ytui.service
    sudo systemctl restart caddy

Progress Tracker
----------------

Wrote a small go helper [ProgressTracker](https://gitlab.com/balki/ytui/-/blob/main/pubsub/pt.go), to watch for progress and update subscribers. Hope it is useful elsewhere too. It has full unit test coverage


Tips
----
## Quick view of the db in command line. 

Needs [yq](https://github.com/mikefarah/yq) and [xsv](https://github.com/burntsushi/xsv) installed

    yq --from-file /dev/fd/7 db.json -o csv 7<<'EOM' | xsv table
    .items
    | del(.[] | .url)
    | (
        [ .[0] | keys ] + 
        [ .[] 
          | .title |= sub("\n", "") 
          | [.*] 
        ]
      )
    EOM

    yq --from-file /dev/fd/7 db.json -o csv 7<<'EOM' | head | xsv table
    .items
    | with(.[]; 
        del(.url),
        .title |= sub("\n", ""),
        .status |= sub("Done", "✓"),
        .status |= sub("Error", "✗"),
        .file_name |= sub("^$", "MISSING")
      )
    | (
        [ .[0] | keys ]
        + [ .[] | [.*] ]
      )
    EOM
