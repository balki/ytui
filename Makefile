
update-bootstrap:
	curl -L "https://cdn.jsdelivr.net/npm/bootstrap@5/dist/css/bootstrap.min.css" -o assets/bootstrap.min.css
	curl -L "https://cdn.jsdelivr.net/npm/bootstrap@5/dist/js/bootstrap.min.js" -o assets/bootstrap.min.js

update-go-deps:
	rm go.sum
	printf "module gitlab.com/balki/ytui\ngo 1.18" > go.mod
	go mod tidy

build:
	go build .

